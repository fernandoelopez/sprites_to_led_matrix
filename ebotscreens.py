from jinja2 import Template
from PIL import Image

template_program='''<?xml version="1.0"?>
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="controls_setupLoop" id="62" deletable="false" x="0" y="0"></block>
  {{ blocks }}
</xml>'''

template_led = '''<block type="MatrixLED_8x8" id="{{ id }}"  disabled="true" x="520" y="{{ (id - 126) * 300  + 65 }}">
    {% set ns = namespace(i=7, j=0) %}
    {% for row in table %}
        {% for pixel in row %}
            <field name="Pixel{{ ns.i }}{{ ns.j }}">{{ 'FF0000' if pixel else '000000' }}</field>
            {% set ns.j = ns.j + 1 %}
        {% endfor %}
        {% set ns.i = ns.i - 1 %}
        {% set ns.j = 0 %}
    {% endfor %}
</block>
'''

template_rgb = '''<block type="MatrixLED" id="{{ id }}"  disabled="true" x="520" y="{{ (id - 126) * 200  + 65 }}">
    {% set ns = namespace(i=0) %}
    {% for row in table %}
        {% for pixel in row %}
            <field name="Pixel{{ ns.i }}">{{ '#{:06x}'.format(pixel) }}</field>
            {% set ns.i = ns.i + 1 %}
        {% endfor %}
    {% endfor %}
</block>
'''


def image_to_frame(image, width, height, rgb=True, resample=Image.LANCZOS):
    matrix = [[0 for i in range(width)] for i in range(height)]
    for row in range(height):
        for col in range(width):
            if rgb:
                r, g, b = image.getpixel((col, row))
                matrix[row][col] = r << 16 | g << 8 | b
            else:
                p = image.getpixel((col, row))
                matrix[row][col] = p
    return matrix


def frames_to_blocks(frames, rgb=True):
    tmp_program = Template(template_program)
    tmp_led = Template(template_led)
    tmp_rgb = Template(template_rgb)
    leds = []
    for i, frame in enumerate(frames, start=126):
        if rgb:
            leds.append(tmp_rgb.render(table=frame, id=i))
        else:
            leds.append(tmp_led.render(table=frame, id=i))

    return tmp_program.render(blocks=''.join(leds))


def images_to_blocks(files, width, height, rgb=True,
                      resample=Image.LANCZOS):
    frames = []
    for file_ in files:
        im = Image.open(file_)
        if rgb:
            im = im.convert('RGB')
        else:
            im = im.convert('1')

        im = im.resize((width, height), resample)
        frames.append(image_to_frame(im, width, height, rgb=rgb,
                      resample=resample))
        im.close()

    return frames_to_blocks(frames, rgb)

def sprites_to_blocks(files, width, height, rgb=True):
    frames = []
    for file_ in files:
        im = Image.open(file_)
        if rgb:
            im = im.convert('RGB')
        else:
            im = im.convert('1')
        for first_row in range(0, im.height, height):
            for first_col in range(0, im.width, width):
                im_c = im.crop((
                    first_col,
                    first_row,
                    first_col + width,
                    first_row + height,
                ))
                frames.append(image_to_frame(im_c, width, height, rgb))
                im_c.close()
        im.close()
    return frames_to_blocks(frames, rgb)
