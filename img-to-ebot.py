from ebotscreens import images_to_blocks, sprites_to_blocks
import argparse
import sys



def parse_args(args):
    parser = argparse.ArgumentParser()
    display_type = parser.add_mutually_exclusive_group()
    display_type.add_argument('--led', action='store_false',
                              dest='rgb')
    display_type.add_argument('--rgb', action='store_true',
                              dest='rgb', default=True)
    parser.add_argument('-o', '--output')
    parser.add_argument('-s', '--sprites', action='store_true')
    parser.add_argument('files', nargs='+')
    return parser.parse_args(args)


def main(args):
    args = parse_args(args)
    print(args)

    if args.output is None:
        outfile = sys.stdout
    else:
        outfile = open(args.output, 'w')

    if args.sprites:
        if args.rgb:
            blocks = sprites_to_blocks(args.files, 8, 5, rgb=True)
        else:
            blocks = sprites_to_blocks(args.files, 8, 8, rgb=False)
    else:
        if args.rgb:
            blocks = images_to_blocks(args.files, 8, 5, rgb=True)
        else:
            blocks = images_to_blocks(args.files, 8, 8, rgb=False)

    outfile.write(blocks)

    if outfile != sys.stdout:
        outfile.close()


if __name__ == '__main__':
    main(sys.argv[1:])
